# 1.0.0 (2022-09-19)


### Bug Fixes

* added chalk colour to cli error ([9e6028c](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/9e6028cc47398fb8db60374e004e8ddb4475af4c))
* added cli usage to readme ([8e39be5](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/8e39be51699c023e432aaacc3873f47f8ad88df9))
* bumped packages versions ([e5509c5](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/e5509c5e3470b5e7fccf177cdf19ae8f4461e724))
* bumped version to trigger release ([4ca5b61](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/4ca5b61f14f7a6fea5d3da553262e96193d4b45c))
* options default to empty object if undefined ([cb9344f](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/cb9344f97ae11193b3a46e74f46fe0a6fe76f397))
* valid cli route exited with a non zero exit code ([d214564](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/d214564085f94b0c59a00ee96da2b3d0e3aaa012))


### Features

* added license and updated package.json ([cd34dc5](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/cd34dc5cddb71f855236594d8bd43aaa572de662))

## [1.1.1](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/compare/v1.1.0...v1.1.1) (2020-04-04)


### Bug Fixes

* valid cli route exited with a non zero exit code ([a86e32f](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/a86e32f74abfd09405e5d43e901ea4afde618cff))

# [1.1.0](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/compare/v1.0.4...v1.1.0) (2020-04-04)


### Features

* added license and updated package.json ([04aa442](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/04aa44206c9a80bffe65aec317a2248c96400538))

## [1.0.4](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/compare/v1.0.3...v1.0.4) (2020-04-04)

## [1.0.3](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/compare/v1.0.2...v1.0.3) (2020-04-04)


### Bug Fixes

* added chalk colour to cli error ([2ed3c1b](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/2ed3c1bb43f7d4f177898a882bba9e843d8db823))
* added cli usage to readme ([109fd64](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/109fd64f3df6e8e1900ad719fbb0048e69ae9244))

## [1.0.2](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/compare/v1.0.1...v1.0.2) (2020-04-04)


### Bug Fixes

* bumped packages versions ([1f05903](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/1f059039460f39f7fb4c802b91c309d17d42ee42))

## [1.0.1](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/compare/v1.0.0...v1.0.1) (2020-04-04)

### Bug Fixes

-   options default to empty object if undefined ([e9a6b9c](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/e9a6b9c7c419c3bcb724f9f7ab3fe9bcd2d20c23))

# 1.0.0 (2020-04-04)

### Bug Fixes

-   bumped version to trigger release ([22990b1](https://gitlab.com/StraightOuttaCrompton/lint-gitlab-ci/commit/22990b1313c79da8a3cd524e8dc509c66cc3d96f))
