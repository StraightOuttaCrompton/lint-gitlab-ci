import { ILintGitlabCiOptions } from "./index";
import { RecursiveRequired } from "assign-default-values-to-object";

const defaultOptions: RecursiveRequired<ILintGitlabCiOptions> = {
    filePath: ".gitlab-ci.yml",
    api: {
        baseUrl: "https://gitlab.com/api",
        version: "v4",
        paths: {
            lintCiYml: "ci/lint",
        },
    },
};

export default defaultOptions;
