import ILintResponse from "../models/ILintResponse";
import IApiLintResponse from "../models/IApiLintResponse";

export default interface IApiResponseParser {
    parseCiLint: (response: IApiLintResponse) => ILintResponse;
}
