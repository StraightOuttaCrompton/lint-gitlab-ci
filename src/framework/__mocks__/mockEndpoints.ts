import IApiEndpoints from "../IApiEndpoints";

export const MOCK_ENDPOINT_RESPONSE = "MOCK_ENDPOINT_RESPONSE: ";

const mockEndpoints: IApiEndpoints = {
    lintCiYml: jest.fn((content) => {
        return new Promise((resolve, reject) => {
            //@ts-ignore
            if (content.length > 0) resolve(MOCK_ENDPOINT_RESPONSE + content);
            else reject({ error: MOCK_ENDPOINT_RESPONSE + "No content" });
        });
    }),
};

export default mockEndpoints;
