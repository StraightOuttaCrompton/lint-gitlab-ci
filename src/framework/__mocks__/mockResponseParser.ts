import IApiResponseParser from "../IApiResponseParser";

export const MOCK_PARSE_CI_LINT_API_RESPONSE = "MOCK_PARSE_CI_LINT_API_RESPONSE: ";

const mockResponseParser: IApiResponseParser = {
    //@ts-ignore
    parseCiLint: jest.fn(() => MOCK_PARSE_CI_LINT_API_RESPONSE),
};

export default mockResponseParser;
