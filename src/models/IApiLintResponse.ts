export default interface IApiLintResponse {
    status: "valid" | "invalid";
    errors: string[];
}
