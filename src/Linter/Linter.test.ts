import Linter from "./Linter";
import mockEndpoints, { MOCK_ENDPOINT_RESPONSE } from "../framework/__mocks__/mockEndpoints";
import mockResponseParser, {
    MOCK_PARSE_CI_LINT_API_RESPONSE,
} from "../framework/__mocks__/mockResponseParser";
import { IMockFiles, mockFiles } from "../__mocks__/mz/mockData";

describe("Linter", () => {
    describe("lintCiYml", () => {
        afterEach(() => {
            jest.clearAllMocks();
        });

        it("should call endpoints with file data as string", async () => {
            const fileName: keyof IMockFiles = "notAString";
            const linter = new Linter(mockEndpoints, mockResponseParser);

            await linter.lintCiYml(fileName);

            expect(mockEndpoints.lintCiYml).toBeCalledWith(mockFiles[fileName].toString());
        });

        it("should return response from api parser", async () => {
            const fileName: keyof IMockFiles = "fileName.yml";
            const linter = new Linter(mockEndpoints, mockResponseParser);

            const result = await linter.lintCiYml(fileName);

            expect(mockResponseParser.parseCiLint).toHaveBeenCalledWith(
                MOCK_ENDPOINT_RESPONSE + mockFiles[fileName]
            );
            expect(result).toEqual(MOCK_PARSE_CI_LINT_API_RESPONSE);
        });
    });
});
