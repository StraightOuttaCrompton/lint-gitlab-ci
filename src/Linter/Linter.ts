import * as fs from "mz/fs";
import IApiEndpoints from "../framework/IApiEndpoints";
import IApiResponseParser from "../framework/IApiResponseParser";
import ILinter from "../framework/ILinter";

export default class Linter implements ILinter {
    private readonly _endpoints: IApiEndpoints;
    private readonly _apiResponeParser: IApiResponseParser;

    constructor(endpoints: IApiEndpoints, apiResponeParser: IApiResponseParser) {
        this._endpoints = endpoints;
        this._apiResponeParser = apiResponeParser;
    }

    public async lintCiYml(fileName: string) {
        const fileData = await fs.readFile(fileName);

        const apiResponse = await this._endpoints.lintCiYml(fileData.toString());

        return this._apiResponeParser.parseCiLint(apiResponse);
    }
}
