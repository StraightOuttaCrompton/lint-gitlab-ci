import ApiEndpoints from "./ApiEndpoints";
import IApiPaths from "./framework/IApiPaths";
import assignDefaultValuesToObject, { RecursiveRequired } from "assign-default-values-to-object";
import Linter from "./Linter/Linter";
import defaultOptions from "./defaultOptions";
import ApiResponseParser from "./ApiResponseParser";

export interface IApiOptions {
    baseUrl?: string;
    version?: string;
    paths?: IApiPaths;
}

export interface ILintGitlabCiOptions {
    filePath?: string;
    api?: IApiOptions;
}

export default function lintGitlabCi(options: ILintGitlabCiOptions = {}) {
    const parsedOptions = assignDefaultValuesToObject(defaultOptions, options) as RecursiveRequired<
        ILintGitlabCiOptions
    >;

    const endpoints = new ApiEndpoints({
        rootUrl: parsedOptions.api.baseUrl,
        apiVersion: parsedOptions.api.version,
        paths: parsedOptions.api.paths,
    });

    const apiResponseParser = new ApiResponseParser();

    const linter = new Linter(endpoints, apiResponseParser);

    return linter.lintCiYml(parsedOptions.filePath);
}
