import IApiResponseParser from "../framework/IApiResponseParser";
import ILintResponse from "../models/ILintResponse";
import IApiLintResponse from "../models/IApiLintResponse";

export default class ApiResponseParser implements IApiResponseParser {
    public parseCiLint(response: IApiLintResponse): ILintResponse {
        return {
            isValid: response.status === "valid" && response.errors.length === 0,
            errors: response.errors,
        };
    }
}
