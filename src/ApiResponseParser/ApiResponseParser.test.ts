import IApiLintResponse from "../models/IApiLintResponse";
import ApiResponseParser from "./ApiResponseParser";

describe("ApiResponseParser", () => {
    describe("valid response", () => {
        it('should set isValid to true when status is "valid"', () => {
            const response: IApiLintResponse = {
                status: "valid",
                errors: [],
            };
            const apiResponseParser = new ApiResponseParser();

            const result = apiResponseParser.parseCiLint(response);

            expect(result.isValid).toBeTruthy();
        });
    });

    describe("invalid response", () => {
        it('should set isValid to false when status is "invalid"', () => {
            const response: IApiLintResponse = {
                status: "invalid",
                errors: [],
            };
            const apiResponseParser = new ApiResponseParser();

            const result = apiResponseParser.parseCiLint(response);

            expect(result.isValid).toBeFalsy();
        });
    });

    describe("errors", () => {
        it("should set isValid to false when errors response contains errors", () => {
            const response: IApiLintResponse = {
                status: "valid",
                errors: ["error"],
            };
            const apiResponseParser = new ApiResponseParser();

            const result = apiResponseParser.parseCiLint(response);

            expect(result.isValid).toBeFalsy();
        });

        it("should set erros array to contain response errorss", () => {
            const response: IApiLintResponse = {
                status: "valid",
                errors: ["error"],
            };
            const apiResponseParser = new ApiResponseParser();

            const result = apiResponseParser.parseCiLint(response);

            expect(result.errors).toBe(response.errors);
        });
    });
});
